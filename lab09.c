#include <stdio.h>
#include <stdlib.h>


typedef void(*vmethod) (void*);
vmethod *mvtable;
vmethod *vtable;

typedef struct Mat_Struct_
{
	int row;
	int col;
	int *array;

	void (*add_m)(void* Mat_Struct, void* Mat_Struct2, void* Mat_Struct3);
	void (*mult_m)(void* Mat_Struct, void* Mat_Struct2, void* Mat_Struct3);
	void (*dis)(void* Mat_Struct);
	vmethod *ptr_vtable;

} Mat_Struct;


typedef struct Vec_Struct_
{
	Mat_Struct M;
	int minimum;
	int maximum;
	vmethod *ptr_vtable;
	
} Vec_Struct;

void add( Mat_Struct* A, Mat_Struct* B, Mat_Struct* result) 
{ 


	if (A->col == B->col && A->row == B->row)
	{
		int i,j;

		result->row = A->row;
		result->col = A->col;
		for (i = 0; i < A->row; i++){
			for (j = 0; j < A->col; j++){
				result->array[i * A->col + j] = A->array[i * A->col + j] + B->array[i * A->col + j];
			}

		}
	}

	else {
		result->row = 0;
	}

	
}

void addv( Mat_Struct* A, Mat_Struct* B, Mat_Struct* result) 
{ 

	if (A->row == B->row)
	{
		int i =0,j;

			for (j = 0; j < A->row; j++){
				result->array[i * A->row + j] = A->array[i * A->row + j] + B->array[i * A->row + j];
			}

	}

	else {
		result->row = 0;
	}

	
}

void mult(Mat_Struct* A, Mat_Struct* B, Mat_Struct* result)
{
	

	if (A->col == B->row)
	{
		int i, j, k;
		int temp=0;
		for (i = 0; i < A->row; i++){
			for (j = 0; j < B->col; j++){
				for (k = 0; k < A->col; k++){
					 temp += A->array[i * A->col + k] * B->array[k * B->col + j];
				}
				
				result->array[i * result->col + j]= temp;
				temp = 0;
			}
		}
	}

	

	else
	{
		result->row = 0;
	}

}

int Mnorm(Mat_Struct* A)
{
	int i, j, lnorm = 0, sum = 0; 

	for(i = 0; i < A->col; i++){
		for(j = 0; j < A->row; j++){
			sum = sum + A->array[j * A->col + i];	
		}
		if(sum > lnorm){
			lnorm = sum;
		}
		sum = 0;
	}

	printf("The L1 norm of the matrix is: %d\n", lnorm);
	return lnorm;
		
}

int Vnorm(Vec_Struct* V)
{
	int i = 0, j, lnorm = 0, sum = 0;

	for(j = 0; j < V->M.row; j++){
		sum = sum + V->M.array[i * V->M.row + j];	
	}

	printf("This L1 norm of the vector is: %d\n", sum);
	return lnorm;
}

void display (Mat_Struct *A)
{
	int i,j;

	for (i = 0; i < A->row; i++){
		for (j = 0; j < A->col; j++){
			printf("%d\t",A->array[i * A->col + j]);
		}
		printf("\n");
	}
}

void init_Mat( Mat_Struct *A, int r, int c)
{
	int i,j;
	A->row = r;
	A->col = c;
	A->array = (int*)malloc(sizeof(int)*r*c);

	A->add_m = &add;
	A->mult_m = &mult;
	A->dis = &display;

	for (i = 0; i <r; i++){
		for (j = 0; j <c; j++){
			A->array[i * c + j] = i * c + j;
		}
	}

}

void init_Vec( Vec_Struct *A, int r)
{
	int i,j;
	A->M.row = r;
	A->M.col = 1;
	A->M.array = (int*)malloc(sizeof(int)*r);

	A->M.add_m = &addv;
	A->M.dis = &display;

	for (i = 0; i < r; i++){
			A->M.array[i] = i;
		}

}


int main (void)
{

	mvtable = malloc(sizeof(vmethod)*4);
	vtable = malloc(sizeof(vmethod)*4);

	Mat_Struct A, result_a, result_m, m_norm;
	init_Mat(&A, 2, 2);
	init_Mat(&result_a, 2, 2);	
	init_Mat(&result_m, 2, 2);
	init_Mat(&m_norm, 2, 2);
	Vec_Struct v, result_v;
	init_Vec(&v, 3);
	init_Vec(&result_v, 3);

	display(&A);
	printf("\n");
	display(&v);
	printf("\n");

	mvtable[0] = &Mnorm;
	A.ptr_vtable = mvtable;
	A.ptr_vtable[0](&A);

	vtable[0] = &Vnorm;
	v.ptr_vtable = vtable;
	v.ptr_vtable[0](&v);

	v.M.add_m(&v, &v, &result_v);
	printf("Addition of two vectors:\n");
	display(&result_v);
	

	A.add_m(&A, &A, &result_a);
	A.mult_m(&A, &A, &result_m);
	
	printf("Addition of two matrices:\n");
	display(&result_a);
	printf("Multiplication of two matrices:\n");
	display(&result_m);


	

}
